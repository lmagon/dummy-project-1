//service-worker.js

this.addEventListener('install', function(event) { //this points to the event listener
  event.waitUntil(//only installs the event listener after the code inside waitUntil has been executed
    caches.open('cache1').then(function(cache) {
      return cache.addAll([ //atomic operation-if any of these fail to cache, none of them are added
        '/dummy-project-1/',
        '/dummy-project-1/webpage.html',
        '/dummy-project-1/webstyles.css',
        '/dummy-project-1/app.js',
        '/dummy-project-1/image-list.js',
        '/dummy-project-1/logo-dog.jpg',
        '/dummy-project-1/gallery/',
        '/dummy-project-1/gallery/share1.jpg',
        '/dummy-project-1/gallery/share2.jpg',
        '/dummy-project-1/gallery/share3.jpg'
      ]);
    })
  );
});

this.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {//cache.match-get something out of the cache
      return response || fetch(event.request).then(function(response) {//return response if already exists in cache. otherwise, go fetch!
        return caches.open('cache1').then(function(cache) {
          cache.put(event.request, response.clone()); //add resource to cache by cloning the response because request and rsponse can only be read once-original gets returned/read to browser,clone gets sentto cache.
          return response;
        }).catch(function () {
        return caches.match('/dummy-project-1/gallery/share3.jpg');
      });
      });
    })
  );
});
