var Path = 'gallery/';

var Gallery = { 'images' : [

  {
    'name'  : 'Story 1',
    'alt' : 'Story 1',
    'url': 'gallery/share1.jpg'
  },

  {
    'name'  : 'Story 2',
    'alt' : 'Story 2',
    'url': 'gallery/share2.jpg'
  },

  {
    'name'  : 'Story 3',
    'alt' : 'Story 3',
    'url': 'gallery/share3.jpg'
  },

]};
